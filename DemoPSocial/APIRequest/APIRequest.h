//
//  APIRequest.h
//  MediaOnTheWord
//
//  Created by admin on 8/1/15.
//  Copyright (c) 2015 DongNguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

@interface APIRequest : NSObject

+(void)getDataFromServerWithUrl:(NSString*)sUrl
                     parameters:(NSString *)parameters
//                      andHeader:(NSDictionary *)DictHeader
             returnValueSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
             returnValueFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

+(void)postDataFromServerWithUrl:(NSString *)sUrl
                      parameters:(NSDictionary*)sParameters
              returnValueSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
              returnValueFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

+(void)deleteDataFromServerWithUrl:(NSString *)sUrl
                        parameters:(NSDictionary*)parameters
                returnValueSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                returnValueFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

@end
