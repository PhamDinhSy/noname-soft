//
//  AppDelegate.h
//  DemoPSocial
//
//  Created by Nguyen Dong on 8/20/15.
//  Copyright (c) 2015 Nguyen Dong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

