//
//  ForgotViewController.h
//  DemoPSocial
//
//  Created by Nguyen Dong on 8/21/15.
//  Copyright (c) 2015 Nguyen Dong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtForgotPw;
- (IBAction)touchSubmitEmailorPhone:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@end
