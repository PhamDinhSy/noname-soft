//
//  CommonFunction.h
//  Instup
//
//  Created by Nguyễn Đông on 8/5/15.
//  Copyright (c) 2015 ducbt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSString (Utilities)
- (BOOL) isEmail;
- (NSString*)trim;
@end

@interface UIImage (Crop)
+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
- (UIImage *)crop:(CGRect)rect;
@end

@interface UIView(Extended)
- (UIImage *) imageByRenderingView;
@end

@interface UIColor (Utilities)
+ (UIColor *)colorFromHexString:(NSString *)hexString;
@end

@interface CommonFunction : NSObject

+ (UIColor *)randomColor;
+(BOOL)isIphone;
+ (BOOL)isPhone5;
+(void)showIndicatorViewOn:(UIView *)aView;
+(void)removeIndcatorViewOn:(UIView *)superView;
+(void)showLoadingViewOn:(UIView *)aView withAlert:(NSString *)text;
+(void)removeLoadingViewOn:(UIView *)superView;
+(NSString*)addDigitIntoNumber:(NSNumber*)input;
+(NSString*)addSuffixTextForNumber:(NSNumber*)input;

+(void)showAlert:(NSString*)title withMessage:(NSString*)msg;
@end
