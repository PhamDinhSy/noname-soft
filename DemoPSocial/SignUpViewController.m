//
//  SignUpViewController.m
//  DemoPSocial
//
//  Created by Nguyen Dong on 8/21/15.
//  Copyright (c) 2015 Nguyen Dong. All rights reserved.
//

#import "SignUpViewController.h"
#import "VerifyViewController.h"
#import "APIRequest.h"
//#import "Static.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer * _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = YES;
    [self.view addGestureRecognizer:_tap];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
}
-(void)hideKeyboard{
    for (UITextField *textField in self.view.subviews) {
        [textField resignFirstResponder];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)beginSignUp{
    if (![self.txtEmail.text isEmail]) {
        [CommonFunction showAlert:@"Error" withMessage:@"Email invalid !"];
    }else if (![self.txtPassword.text isEqualToString:self.txtConfirmPassword.text]){
        [CommonFunction showAlert:@"Error" withMessage:@"Password not match !"];
    }else {
        [CommonFunction showLoadingViewOn:self.view withAlert:nil];
        NSString *strURL = [NSString stringWithFormat:@"%@/SignIn",BODY_URL];
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        [dictParameters setObject:self.txtFistName.text forKey:@"firstName"];
        [dictParameters setObject:self.txtLastName.text forKey:@"lastName"];
        [dictParameters setObject:self.txtEmail.text forKey:@"email"];
        [dictParameters setObject:self.txtPassword.text forKey:@"password"];
        [dictParameters setObject:self.txtConfirmPassword.text forKey:@"confirm_password"];
        [APIRequest postDataFromServerWithUrl:strURL parameters:dictParameters returnValueSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Success .
            [CommonFunction removeLoadingViewOn:self.view];
        } returnValueFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Fail.
            [CommonFunction removeLoadingViewOn:self.view];
            int count = arc4random() %2;
            switch (count) {
                case 0:
                    // random Fail
                    [CommonFunction showAlert:@"Error" withMessage:error.description];
                    break;
                case 1:
                    // random Success
                    [self signUpSuccess];
                    break;
                    
                default:
                    break;
            }
        }];
    }
}
-(void)signUpSuccess{
    VerifyViewController *verifyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyViewController"];
    [self.navigationController pushViewController:verifyVC animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchSingUpButton:(id)sender {
    [self beginSignUp];
}
@end
