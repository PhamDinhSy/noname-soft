//
//  SignInViewController.h
//  DemoPSocial
//
//  Created by Nguyen Dong on 8/21/15.
//  Copyright (c) 2015 Nguyen Dong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnSingIn;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtEmailOrPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPw;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
- (IBAction)touchSignInButton:(id)sender;
@end
