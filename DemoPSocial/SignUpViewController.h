//
//  SignUpViewController.h
//  DemoPSocial
//
//  Created by Nguyen Dong on 8/21/15.
//  Copyright (c) 2015 Nguyen Dong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtFistName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;

- (IBAction)touchSingUpButton:(id)sender;
@end
