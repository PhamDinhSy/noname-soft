//
//  VerifyViewController.h
//  DemoPSocial
//
//  Created by Nguyen Dong on 8/21/15.
//  Copyright (c) 2015 Nguyen Dong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *txtCodeVerify;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)touchSubmitVerify:(id)sender;
@end
