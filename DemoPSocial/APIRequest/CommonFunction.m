//
//  CommonFunction.m
//  Instup
//
//  Created by Nguyễn Đông on 8/5/15.
//  Copyright (c) 2015 ducbt. All rights reserved.
//

#import "CommonFunction.h"

#define AppNameTitle @"ExpensesEntry"
#define IS_HEIGHT_GTE_568 [[UIScreen mainScreen ] bounds].size.height >= 568.0f

@implementation UIImage (Crop)

+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (UIImage *)crop:(CGRect)rect {
    if (self.scale > 1.0f) {
        rect = CGRectMake(rect.origin.x * self.scale,
                          rect.origin.y * self.scale,
                          rect.size.width * self.scale,
                          rect.size.height * self.scale);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}
@end

const NSString *emailRegEx =
@"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
@"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
@"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
@"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
@"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
@"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
@"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
const NSString *numberRegEx = @"[0-9]+";
@implementation UIView(Extended)
- (UIImage *) imageByRenderingView {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0.0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
@end


@implementation NSString (Utilities)
- (BOOL) isEmail {
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return [regExPredicate evaluateWithObject:self];
}
- (NSString*)trim {
    return [self stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end

@implementation UIColor (Utilities)

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end


@implementation CommonFunction
+(void)showAlert:(NSString*)title withMessage:(NSString*)msg {
    if (!title || [title isEqual: @""]) {
        title = AppNameTitle;
    }
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
    alertView = nil;
}
+ (UIColor *)randomColor{
    CGFloat red = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat green = ( arc4random() % 256 / 256.0 );  //  0.5 to 1.0, away from white
    CGFloat blue = ( arc4random() % 256 / 256.0 );  //  0.5 to 1.0, away from black
    
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1];
    return color;
}

+(BOOL)isIphone {
    //NSString *deviceType = [UIDevice currentDevice].model;
    //return [deviceType isEqualToString:@"iPhone"];
    return !(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad);
}

+ (BOOL)isPhone5{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if (IS_HEIGHT_GTE_568) {
            return YES;
        }
        return NO;
    }
    return NO;
}
+(void)showIndicatorViewOn:(UIView *)aView{
    UIActivityIndicatorView *activityIndication = [[UIActivityIndicatorView alloc] init];
    activityIndication.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    int width = [CommonFunction isIphone]?20:30;
    activityIndication.frame = CGRectMake((aView.frame.size.width - width)/2,
                                          (aView.frame.size.height-width)/2,
                                          width,
                                          width);
    activityIndication.tag = 1012;
    [activityIndication startAnimating];
    activityIndication.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    
    
    [aView addSubview:activityIndication];
}
+(void)removeIndcatorViewOn:(UIView *)superView{
    for (UIView *aView in superView.subviews) {
        if ((aView.tag == 1012)  && [aView isKindOfClass:[UIActivityIndicatorView class]]) {
            [aView removeFromSuperview];
        }
    }
}
+(void)showLoadingViewOn:(UIView *)aView withAlert:(NSString *)text{
    UIView *loadingView = [[UIView alloc] init];
    loadingView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin|
    UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    loadingView.frame = ([CommonFunction isIphone])?CGRectMake(0, 0, aView.frame.size.width, aView.frame.size.height):CGRectMake(0, 0, aView.frame.size.width, aView.frame.size.height-80);
    loadingView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0];
    loadingView.tag = 1011;
    UILabel *loadingLabel = [[UILabel alloc ] init];
    
    UIView* roundedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 80)];
    roundedView.center = CGPointMake(loadingView.frame.size.width/2, loadingView.frame.size.height/2);
    roundedView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    roundedView.layer.borderColor = [UIColor clearColor].CGColor;
    roundedView.layer.borderWidth = 1.0;
    roundedView.layer.cornerRadius = 10.0;
    [loadingView addSubview:roundedView];
    roundedView.autoresizingMask = loadingLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    
    loadingLabel.text = text;
    loadingLabel.frame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y + 50,100, 30);
    //loadingLabel.adjustsFontSizeToFitWidth = YES;
    loadingLabel.textAlignment = NSTextAlignmentCenter;
    loadingLabel.font = [UIFont boldSystemFontOfSize:14];
    loadingLabel.backgroundColor = [UIColor clearColor];
    loadingLabel.textColor = [UIColor whiteColor];
    [loadingView addSubview:loadingLabel];
    
    UIActivityIndicatorView *activityIndication = [[UIActivityIndicatorView alloc] init];
    activityIndication.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    activityIndication.frame = CGRectMake((loadingView.frame.size.width - 30)/2,
                                          roundedView.frame.origin.y + 15,
                                          30,
                                          30);
    
    [activityIndication startAnimating];
    [loadingView addSubview:activityIndication];
    activityIndication.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    
    //	[activityIndication release];
    [aView addSubview:loadingView];
}
+(void)removeLoadingViewOn:(UIView *)superView{
    for (UIView *aView in superView.subviews) {
        if ((aView.tag == 1011)  && [aView isKindOfClass:[UIView class]]) {
            [aView removeFromSuperview];
        }
    }
}

+(NSString*)addDigitIntoNumber:(NSNumber*)input
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    [formatter setGroupingSeparator:groupingSeparator];
    [formatter setGroupingSize:3];
    [formatter setAlwaysShowsDecimalSeparator:NO];
    [formatter setUsesGroupingSeparator:YES];
    
    return [formatter stringFromNumber:input];
}

+(NSString*)addSuffixTextForNumber:(NSNumber*)input
{
    if (!input)
        return @"";
    
    long long num = [input longLongValue];
    
    int s = ( (num < 0) ? -1 : (num > 0) ? 1 : 0 );
    NSString* sign = (s == -1 ? @"-" : @"" );
    
    num = llabs(num);
    
    if (num < 1000)
        return [NSString stringWithFormat:@"%@%lld",sign,num];
    
    int exp = (int) (log(num) / log(1000));
    
    NSArray* units = @[@"K",@"M",@"G",@"T",@"P",@"E"];
    
    return [NSString stringWithFormat:@"%@%.1f%@",sign, (num / pow(1000, exp)), [units objectAtIndex:(exp-1)]];
}
@end
