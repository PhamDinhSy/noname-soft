//
//  APIRequest.m
//  MediaOnTheWord
//
//  Created by admin on 8/1/15.
//  Copyright (c) 2015 DongNguyen. All rights reserved.
//

#import "APIRequest.h"
#define TIME_OUT 20.0f

@implementation APIRequest

+(void)getDataFromServerWithUrl:(NSString*)sUrl
                     parameters:(NSString *)sParameters
//                      andHeader:(NSDictionary *)DictHeader
             returnValueSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
             returnValueFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *URL = [[NSString stringWithFormat:@"%@?%@", sUrl, sParameters] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:TIME_OUT];
    
    [manager GET:URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}


+(void)postDataFromServerWithUrl:(NSString *)sUrl
                         parameters:(NSDictionary*)sParameters
              returnValueSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
              returnValueFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *URL = [sUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [manager.requestSerializer setTimeoutInterval:TIME_OUT];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager POST:URL parameters:sParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}

+(void)deleteDataFromServerWithUrl:(NSString *)sUrl
                        parameters:(NSDictionary*)parameters
                returnValueSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                returnValueFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *URL = [sUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [manager.requestSerializer setTimeoutInterval:TIME_OUT];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager DELETE:URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}

@end
