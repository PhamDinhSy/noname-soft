//
//  VerifyViewController.m
//  DemoPSocial
//
//  Created by Nguyen Dong on 8/21/15.
//  Copyright (c) 2015 Nguyen Dong. All rights reserved.
//

#import "VerifyViewController.h"
#import "APIRequest.h"

@interface VerifyViewController ()

@end

@implementation VerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer * _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    _tap.enabled = YES;
    [self.view addGestureRecognizer:_tap];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
}

-(void)hideKeyboard{
    for (UITextField *textField in self.view.subviews) {
        [textField resignFirstResponder];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)touchSubmitVerify:(id)sender {
    [self beginSubmitVerifyCode];
}
-(void)beginSubmitVerifyCode{
    NSString *strURL = [NSString stringWithFormat:@"%@/verify",BODY_URL];
    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
    [dictParameters setObject:self.txtCodeVerify.text forKey:@"code"];
    [APIRequest postDataFromServerWithUrl:strURL parameters:dictParameters returnValueSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Success .
        [CommonFunction removeLoadingViewOn:self.view];
    } returnValueFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Fail.
        [CommonFunction removeLoadingViewOn:self.view];
        int count = arc4random() %2;
        switch (count) {
            case 0:
                // random Fail
                [CommonFunction showAlert:@"Error" withMessage:error.description];
                break;
            case 1:
                // random Success
                [self submitSuccess];
                break;
                
            default:
                break;
        }
    }];

}
-(void)submitSuccess{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
